<#
    .SYNOPSIS
        Downloads and configures SSM.
#>
 
# Firewall
#netsh advfirewall firewall add rule name="port_25689" dir=in action=allow protocol=TCP localport=25689

# Folders
New-Item -ItemType Directory c:\temp
New-Item -ItemType Directory c:\ProDVD
 
# Download ssm app
Invoke-WebRequest  https://bitbucket.org/shuhrat100/test/raw/9c7fb40381b051d96c88bf3508a39f191afc5bce/SSM.msi -OutFile c:\ProDVD\SSM.msi
  
Start-Process msiexec.exe -ArgumentList '/I C:\ProDVD\SSM.msi /quiet'

Start-Process 'C:\Program Files (x86)\ProDVD\SSM\ProDVD_SSM_UI.exe'